import os

class Config:
    host = os.getenv('FLASK_HOST', '0.0.0.0')
    port = int(os.getenv('FLASK_PORT', 8080))
    debug = int(os.getenv('FLASK_DEBUG', 1))